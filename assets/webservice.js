import {
    NetInfo,
  } from 'react-native';
import constant from "./constatant" 
import userDefaults from 'react-native-default-preference';
     
  const URL = constant.URL;
  export default class WebServiceHandler {
    //=================================================//
    //     function for login                          //
    //=================================================//
    static login(parameter){
      return new Promise(function(success, failed){
        NetInfo.isConnected.fetch().done((isConnected) => {
              console.log('WebServiceHandler: Network Connectivity status: ' + isConnected);
              if(isConnected) {
                fetch(URL+'manager/login', {
                    method: 'POST',
                    headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json'
                    }, body:parameter,
                }) .then(function(response) {
                  console.log(response);
                    return response.json();
                })
                .then(function(jsonResponse) {
                  return success(jsonResponse);
                })
                .catch(function(err) {
                      failed(err)
                });
              }else {
                alert("No Internet connection")
              }
            });
      });
    };


    //=================================================//
    //     function for Save Complaint              //
    //=================================================//
    static getComplaintlist(parameter){
      console.log('WebServiceHandler:Initiating PUT request');
      return new Promise(function(success, failed){
        NetInfo.isConnected.fetch().done((isConnected) => {
              console.log('WebServiceHandler: Network Connectivity status: ' + isConnected);
              if(isConnected) {
                userDefaults.get('token').then((value) => {
                  fetch(URL+'managerComplaint', {
                      method: 'PUT',
                      headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': "JWT "+value
                      }, body:parameter,
                  }) .then(function(response) {
                    console.log(response);
                      return response.json();
                  })
                  .then(function(jsonResponse) {
                    return success(jsonResponse);
                  })
                  .catch(function(err) {
                        failed(err)
                  });
                });  
              }else {
              alert("No Internet connection")
              }
          });
      });
    };

    //=================================================//
    //     function for get User detail                //
    //=================================================//
    static getUserdetail(parameter){
      console.log('WebServiceHandler:Initiating POST request');
      return new Promise(function(success, failed){
        NetInfo.isConnected.fetch().done((isConnected) => {
              console.log('WebServiceHandler: Network Connectivity status: ' + isConnected);
              
              if(isConnected) {
                userDefaults.get('token').then((value) => {
                fetch(URL+'customer/userProfile/'+ parameter, {
                    method: 'GET',
                    headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                      'Authorization':  "JWT "+value
                    }
                }) .then(function(response) {
                  console.log(response);
                    return response.json();
                })
                .then(function(jsonResponse) {
                  return success(jsonResponse);
                })
                .catch(function(err) {
                      failed(err)
                });
                });
              } else {
              alert("No Internet connection")
              }
          });
      });
    };
    
    //=================================================//
    //     function for SendOtp  in mail               //
    //=================================================//
    static sendOtp(parameter){
      console.log('WebServiceHandler:Initiating POST request');
      return new Promise(function(success, failed){
        NetInfo.isConnected.fetch().done((isConnected) => {
              console.log('WebServiceHandler: Network Connectivity status: ' + isConnected);
              if(isConnected) {
              fetch(URL+'customer/forgotPassword', {
                  method: 'POST',
                  headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                  }, body:parameter,
              }) .then(function(response) {
                console.log(response);
                  return response.json();
              })
              .then(function(jsonResponse) {
                return success(jsonResponse);
              })
              .catch(function(err) {
                    failed(err)
              });
              }else {
              alert("No Internet connection")
              }
          });
      });
    };

    //=================================================//
    //     function for save Forget Password           //
    //=================================================//
    static saveForgetPassword(parameter){
      console.log('WebServiceHandler:Initiating POST request' , parameter );
      return new Promise(function(success, failed){
        NetInfo.isConnected.fetch().done((isConnected) => {
              console.log('WebServiceHandler: Network Connectivity status: ' + isConnected);
              if(isConnected) {
              fetch(URL+'customer/forgotPassword', {
                  method: 'PUT',
                  headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                  }, body:parameter,
              }) .then(function(response) {
                console.log(response);
                  return response.json();
              })
              .then(function(jsonResponse) {
                return success(jsonResponse);
              })
              .catch(function(err) {
                    failed(err)
              });
              }else {
              alert("No Internet connection")
              }
          });
      });
    };
    //=================================================//
    //     function for save Change password                          //
    //=================================================//
    static saveChangepassword(parameter){
      console.log('WebServiceHandler:Initiating POST request');
      return new Promise(function(success, failed){
        NetInfo.isConnected.fetch().done((isConnected) => {
              console.log(parameter);
              console.log('WebServiceHandler: Network Connectivity status: ' + isConnected);
              if(isConnected) {
                userDefaults.get('token').then((value) => {
                fetch(URL+'change-password', {
                    method: 'POST',
                    headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                      'Authorization': "JWT "+value
                    }, body:parameter,
                }) .then(function(response) {
                  console.log(response);
                    return response.json();
                })
                .then(function(jsonResponse) {
                  return success(jsonResponse);
                })
                .catch(function(err) {
                      failed(err)
                });
              }); 
              }else {
               alert("No Internet connection")
              }
          });
      });
    };

    
    //=================================================//
    //     function for sentMobileotp                  //
    //=================================================//
    static sentMobileotp(parameter){
      console.log('WebServiceHandler:Initiating POST request');
      return new Promise(function(success, failed){
        NetInfo.isConnected.fetch().done((isConnected) => {
              console.log('WebServiceHandler: Network Connectivity status: ' + isConnected);
              if(isConnected) {
              fetch(URL+'user-save-phone-number', {
                  method: 'POST',
                  headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json'
                  }, body:parameter,
              }) .then(function(response) {
                console.log(response);
                  return response.json();
              })
              .then(function(jsonResponse) {
                return success(jsonResponse);
              })
              .catch(function(err) {
                    failed(err)
              });
              }else {
              alert("No Internet connection")
              }
          });
      });
    };
    

    //=================================================//
    //     function for edit user profile              //
    //=================================================//
    static updateUserprofile(parameter,uId){
      console.log('WebServiceHandler:Initiating PUT request');
      return new Promise(function(success, failed){
        NetInfo.isConnected.fetch().done((isConnected) => {
              console.log('WebServiceHandler: Network Connectivity status: ' + isConnected);
              if(isConnected) {
                userDefaults.get('token').then((value) => {
                  fetch(URL+'customer/userProfile/'+uId, {
                      method: 'PUT',
                      headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': "JWT "+value
                      }, body:parameter,
                  }) .then(function(response) {
                    console.log(response);
                      return response.json();
                  })
                  .then(function(jsonResponse) {
                    return success(jsonResponse);
                  })
                  .catch(function(err) {
                        failed(err)
                  });
                })
              }else {
              alert("No Internet connection")
              }
          });
      });
    };

    //=================================================//
    //     function for Save Complaint              //
    //=================================================//
    static SaveComplaint(parameter){
      console.log('WebServiceHandler:Initiating POST request');
      return new Promise(function(success, failed){
        NetInfo.isConnected.fetch().done((isConnected) => {
              console.log('WebServiceHandler: Network Connectivity status: ' + isConnected);
              if(isConnected) {
                userDefaults.get('token').then((value) => {
                  fetch(URL+'complaint', {
                      method: 'POST',
                      headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': "JWT "+value
                      }, body:parameter,
                  }) .then(function(response) {
                    console.log(response);
                      return response.json();
                  })
                  .then(function(jsonResponse) {
                    return success(jsonResponse);
                  })
                  .catch(function(err) {
                        failed(err)
                  });
                });  
              }else {
              alert("No Internet connection")
              }
          });
      });
    };

    //===========================================//
    //     function for upvote Update            //
    //===========================================//
    static upvoteUpdate(parameter){
      console.log('WebServiceHandler:Initiating POST request');
      return new Promise(function(success, failed){
        NetInfo.isConnected.fetch().done((isConnected) => {
              console.log("parameter",parameter)
              console.log('WebServiceHandler: Network Connectivity status: ' + isConnected);
              if(isConnected) {
                userDefaults.get('token').then((value) => {
                  fetch(URL+'complaint/upVote', {
                      method: 'POST',
                      headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': "JWT "+value
                      }, body:parameter,
                  }) .then(function(response) {
                    console.log(response);
                      return response.json();
                  })
                  .then(function(jsonResponse) {
                    return success(jsonResponse);
                  })
                  .catch(function(err) {
                        failed(err)
                  });
                });  
              }else {
              alert("No Internet connection")
              }
          });
      });
    };
     //===========================================//
    //     function for Save  Comment            //
    //===========================================//
    static SaveComment(parameter){
      console.log('WebServiceHandler:Initiating POST request');
      return new Promise(function(success, failed){
        NetInfo.isConnected.fetch().done((isConnected) => {
              console.log("parameter",parameter)
              console.log('WebServiceHandler: Network Connectivity status: ' + isConnected);
              if(isConnected) {
                userDefaults.get('token').then((value) => {
                  fetch(URL+'complaint/upVote', {
                      method: 'PUT',
                      headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': "JWT "+value
                      }, body:parameter,
                  }) .then(function(response) {
                    console.log(response);
                      return response.json();
                  })
                  .then(function(jsonResponse) {
                    return success(jsonResponse);
                  })
                  .catch(function(err) {
                        failed(err)
                  });
                });  
              }else {
              alert("No Internet connection")
              }
          });
      });
    };

    //===========================================//
    //     function for Get  Comment list        //
    //===========================================//
    static getCommentList(parameter){
      console.log('WebServiceHandler:Initiating POST request');
      return new Promise(function(success, failed){
        NetInfo.isConnected.fetch().done((isConnected) => {
              console.log("parameter",parameter)
              console.log('WebServiceHandler: Network Connectivity status: ' + isConnected);
              if(isConnected) {
                userDefaults.get('token').then((value) => {
                  fetch(URL+'complaint/comment', {
                      method: 'POST',
                      headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': "JWT "+value
                      }, body:parameter,
                  }) .then(function(response) {
                    console.log(response);
                      return response.json();
                  })
                  .then(function(jsonResponse) {
                    return success(jsonResponse);
                  })
                  .catch(function(err) {
                        failed(err)
                  });
                });  
              }else {
              alert("No Internet connection")
              }
          });
      });
    };
    
    
    
}