import React from "react";
import { Dimensions, Image,TouchableOpacity, StatusBar } from "react-native";
import {
  Button,
  Text,
  Container,
  List,
  ListItem,
  Content, 
  View,
  CardItem,
  Left,
  Right,
  Body
} from "native-base";
import userDefaults from 'react-native-default-preference'
import WebServiceHandler  from '../../webservice';
import Icon from "react-native-vector-icons/MaterialIcons";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import Entypo from "react-native-vector-icons/Entypo";
const routes = [{route:"Home",name:"Home",iconname:"home"},
                {route:"Editprofile",name:"Edit profile",iconname:"edit"},
                {route:"ChangePassword",name:"Change password",iconname:"lock"},
                {route:"TermsConditions" ,name:"Term and condition",iconname:"book"},
                {route:"AboutUs" ,name:"About us" ,iconname:"people"},
                {route:"FAQ",name:"FAQ",iconname:"question-answer"},
                
              ];

const { width, height } = Dimensions.get('window');                
export default class SideBar extends React.Component {

  constructor(props) {
    console.log(props);
      super(props);
      this.state = {
        user_data:[],
        image:null,
        fullName:null,
        userPhone:null,
      }
  }
  // getUserdetail = () => {
  //     WebServiceHandler.getUserdetail(this.state.user_data.customer_id)
  //     .then(response => { 
  //     if (response.success) { 
  //       console.log("response.success",response.success);
  //       this.setState({
  //         fullName: response.success.data.civilit + ' ' + response.success.data.firstname + ' ' + response.success.data.lastname,
  //         image:response.success.data.image,
  //         userPhone:response.success.data.mobile,
  //       })
  //     } else {                      
  //       let msg = response.msg;
  //       alert(msg)
  //     }
  //     }).catch(error => {
  //       alert(error)
  //   });       
  // } 
  // getUserprofile =() =>{
  //   userDefaults.get('user_data').then((value) => {
  //     this.setState({ user_data : JSON.parse(value)})
  //     this.getUserdetail();
  //   });
  // }

  // componentWillMount() {
  //   this.getUserprofile();
  // }
  // componentWillReceiveProps(){
  //   this.getUserprofile();
  // }


  render() {
    return (
      <Container>
        <Content>
            <ListItem  style={{backgroundColor:'blue',marginLeft:0, height:120}}>
                <View >
                      <Image
                        square
                        style={{
                          height: 75,
                          width: 75,
                          borderColor:'grey',
                          borderWidth:3,
                          marginLeft:5,
                          borderRadius:45,
                          marginTop:10,
                          marginHorizontal:5
                        }}
                        source={{
                          uri:"http://liveinloops.net/app/backoffice/public/default_avatar.jpg"
                        }}
                      />
                </View>
                <View >
                <View  style={{flexDirection:'column', marginTop:13}}>
                      <Text style={{color:'white',}}>Manager</Text>
                </View>  
                </View>
            </ListItem>
            <List
            dataArray={routes}
            contentContainerStyle={{  }}
            renderRow={data => {
              return (
                <ListItem
                  button
                  onPress={() => this.props.navigation.navigate(data.route)}
                > 
                  <Icon  name ={data.iconname} size={20}  />
                  <Text style={{marginLeft:10}}>{data.name}</Text>
                </ListItem>
              );
            }}
          />
        </Content>
        <ListItem
                  button
                  onPress={() =>  userDefaults.clearAll().then(this.props.navigation.dispatch({
                    type: 'Navigation/RESET',
                    index: 0,
                    actions: [{ type: 'Navigate', routeName: 'Login' }]
                }))}
                > 
                  <Icon  name ="power" size={20}  />
                  <Text style={{marginLeft:10}}>Logout</Text>
                  
                  <Text style={{marginLeft:5,marginTop:50,fontSize:13}}>{'\n'}V 1.01</Text>
        </ListItem>
      </Container>
    );
  }
}
