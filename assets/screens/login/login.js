//=============================================/
//       Login page fnctionallity             //
//=============================================/

import React, { Component } from "react";
import { Image, Dimensions,ImageBackground, StatusBar,ActivityIndicator, Platform, Modal } from "react-native";
import {
  Container,
  Content,
  Button,
  Left,
  Item,
  Input,
  Form,
  View,
  Text,
  Title,
  Footer,
	Body,
  Right,
  Header
} from "native-base";
import Icon from "react-native-vector-icons/MaterialIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import WebServiceHandler from "../../webservice";
import styles from "./styles";
import commonstyle from "../../commonstyle";
import userDefaults from 'react-native-default-preference';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import constant from "../../constatant" ;
// import WebServiceHandler from '../../webservice';
export default class Login extends Component {
  static navigationOptions = {
    header: null
  };

//=============================================/
//        constructor for defualt value        //
//=============================================/
  constructor(props) {
    super(props);
    
    this.state = {
        icEye: 'visibility-off',
        pEye: 'visibility-off',
        cpEye: 'visibility-off',
        securepassword: true,
        secureconfpassword: true,
        email: '',
        newpassword:'',
        password: '',
        conpassword: '',
        mobileno:'',
        email:'',
        otpno:'',
        isLoading: false,
        ForgetPassmodalVisible:false,
        sendOtpstatus:false,
        verifyOtpstatus:false,
        numberError: false
    }
  }

//=============================================/
//        Change password visibility          //
//=============================================/

// componentWillMount(){
//   userDefaults.get('token').then((value) => {
//     alert(value);
//   });  
// }

changePwdType = () => {
    let newState;
    if (this.state.securepassword) {
        newState = {
            icEye: 'visibility',
            securepassword: false
        }
    } else {
        newState = {
            icEye: 'visibility-off',
            securepassword: true
        }
    }
   
  
    // set new state value
    this.setState(newState)

};

setPwd = () => {
  let newPassState;
  if (this.state.securepassword) {
    newPassState = {
        pEye: 'visibility',
        securepassword: false
      }
  } else {
    newPassState = {
        pEye: 'visibility-off',
        securepassword: true
      }
  }
   // set new password state value
   this.setState(newPassState)
};

setConfPwd = () => {
  let newConfPassState;
  if (this.state.secureconfpassword) {
      newConfPassState = {
          cpEye: 'visibility',
          secureconfpassword: false
      }
  } else {
      newConfPassState = {
          cpEye: 'visibility-off',
          secureconfpassword: true
      }
  }

  // set new newConfPassState state value
  this.setState(newConfPassState)

}; 

//=============================================/
//        email and password Text Changed      //
//=============================================/
mobnumberTextChanged = (event) => {
  this.setState({ email: event.nativeEvent.text })
}

passwordTextChanged = (event) => {
  this.setState({ password: event.nativeEvent.text })
}
//=============================================/
//   Forget password change functionallity     //
//=============================================/
setForgePasstModalVisible = (visible) => {
  if(visible == false){
    this.setState({ icEye: 'visibility-off',
                    pEye: 'visibility-off',
                    cpEye: 'visibility-off',
                    securepassword: true,
                    secureconfpassword: true,
                    email: '',
                    newpassword:'',
                    password: '',
                    conpassword: '',
                    emailid:'',
                    otpno:'',
                    isLoading: false,
                    ForgetPassmodalVisible:false,
                    selected1:'',
                    sendOtpstatus:false,
                    verifyOtpstatus:false,
                    user_id:''});
  }else{
    this.setState({ForgetPassmodalVisible: visible});
  }
}


VerifyOtp = () => {
  if(this.state.otpno == ''){
     alert("please enter 4 digit OTP number send in your registered Email id");
   }
  else if(this.state.checkOtp != this.state.otpno){
    alert("please enter valid OTP number");
  }else{
    this.setState({ sendOtpstatus:false,
                    verifyOtpstatus :true
                  });
    
  }
}

//=============================================/
//   sendOtp in email functiona                 //
//=============================================/
sendOtp = () => {
  if (this.state.email == '') {
    alert("Please enter mobile number")      
  } else {
    this.setState({ isLoading: true });
      let data = JSON.stringify({
                  mobile: this.state.email,
                  pId:constant.PROPERTY_ID,
                });        
        WebServiceHandler.sendOtp(data)
        .then(response => { 
        console.log('otp response', response)
        if (response.success) {   
          this.setState({ 
                          checkOtp:response.success.data.otp,
                          user_id:response.success.data.customer_id,
                          sendOtpstatus:true,
                          isLoading: false
                        });
          alert("OTP send succesfully!");
        } else {   
          this.setState({ isLoading: false });                      
          let msg = response.msg;
          console.log('message', msg)
          alert(msg)
        }
      }).catch(error => {
        this.setState({ isLoading: false });
        alert(error)
      });    
  }
}

//==================================================================/
//mobile number, verify otp password,confirmpassword Text Changed   //
//==================================================================/
emailrTextChanged = (event) => {
  this.setState({ email: event.nativeEvent.text })
}

otpTextChanged = (event) => {
  this.setState({ otpno: event.nativeEvent.text })
}

newpasswordTextChanged = (event) => {
  this.setState({ newpassword: event.nativeEvent.text })
} 

conPasswordtextChanged = (event) => {
  this.setState({ conpassword: event.nativeEvent.text })
} 

//=============================================/
//        function for login (loginPressed)   //
//=============================================/
loginPressed = () => {
  this.setState({numberError: false});
  this.setState({passwordError: false}); 
  if(this.state.email == '' && this.state.password == '') {
    this.setState({numberError: true});
    this.setState({passwordError: true}); 
  }
  if (this.state.email == '') {
    //alert('Enter your mobile number');
    this.setState({numberError: true});
  } else if (this.state.password == '') {
    // alert('Please enter password')     
    this.setState({passwordError: true}); 
  } else {
      this.setState({ isLoading: true });
      let data = JSON.stringify({
                  email: this.state.email,
                  password: this.state.password,
                  pId: constant.PROPERTY_ID,
                });
      WebServiceHandler.login(data).then(response => {    
        if (response.success) { 
          userDefaults.set("token", response.success.data.token);
          userDefaults.set("user_first_name", response.success.data.first_name);
          userDefaults.set("user_last_name", response.success.data.last_name);
          userDefaults.set("user_email", response.success.data.email);
          userDefaults.set("customer_id", JSON.stringify(response.success.data.usrDetail.id));
          userDefaults.set("property_name", JSON.stringify(response.success.data.usrDetail.property_name));
          userDefaults.set("address", JSON.stringify(response.success.data.usrDetail.address));
          // userDefaults.set("first_name", response.success.data.first_name);
          // userDefaults.set("last_name", response.success.data.last_name);
          this.setState({ isLoading: false });
          this.props.navigation.navigate('HomeScreen');
        } else {   
          this.setState({ isLoading: false });                   
          let msg ="Mobile number or password are wrong";
          alert(msg)
        }
      }).catch(error => {
        this.setState({ isLoading: false });
        alert(error)
      });    
  }
}
//*************************************************/
//     function for forget password (saveForgetPassword)       //
//*************************************************/
saveForgetPassword = () => {
  if (this.state.newpassword == '') {
    alert('Please enter password')  
  }else if (this.state.conpassword == '') {
    alert('Please enter confirm password');
  }else if (this.state.newpassword != this.state.conpassword) {
    alert('Please enter confirm password same as password');
  } else {
    this.setState({ isLoading: true });
       let data = JSON.stringify({
          id: this.state.user_id,
          password: this.state.newpassword,
          pId: constant.PROPERTY_ID
        });
        WebServiceHandler.saveForgetPassword(data)
        .then(response => { 
        if (response.success) {   
          this.setState({ ForgetPassmodalVisible:false, 
                          isLoading: false
                        });
          alert('Your password has been reset successfully!');
        } else { 
          this.setState({ isLoading: false });                     
          let msg = response.msg;
          alert(msg)
        }
      }).catch(error => {
        this.setState({ isLoading: false });
        alert(error)
      });       
  }
}

onLayout = (e) => {
  let newState = {
    screeHW: Dimensions.get('window')
  }
  this.setState({orientation: newState.screeHW.width > newState.screeHW.height ? 'PORTRAIT' : 'LANDSCAPE'});
  //alert(newState.screeHW.width > newState.screeHW.height ? 'PORTRAIT' : 'LANDSCAPE')
  this.setState(newState);
}
//=================================================/
//            Render to login page                //
//=================================================/
  render() {
    const { navigate } = this.props.navigation;
    let {width, height} = Dimensions.get('window');
    return (
          
      <Container 
        //  style={{backgroundColor:'#5b6567'}}
      >
       <ImageBackground source={require('../../media/images/home.jpg')} style={{ flex: 1,
        width: width,
        height: height,
        justifyContent: 'center',
        alignItems: 'center',}}> 
       <View onLayout={this.onLayout.bind(this)} style={{alignContent: 'center', alignItems: 'center', alignSelf: 'center'}}>
            <Image  source={require('../../media/images/Smarttown.png')} style={{maxHeight: height*0.45, maxWidth: width}} />
          </View>
        <Content style={{ padding: 20 }}>
         
          <View 
            style={{ marginBottom:15 }}
          >
            <Form>
              <Item rounded style={[{marginBottom: 15},commonstyle.borderstyle]}>
              <MaterialCommunityIcons style={[{ left: 20},commonstyle.iconstyle]}
                      size={this.props.iconSize}
                      name="email"
                />
               <Input
                    placeholderTextColor ='white'
                    placeholder='Email'
                    style={commonstyle.loginputstyle}
                    value={this.state.email}
                    returnKeyType={"done"}
                    onChange={this.emailrTextChanged}
                />
              </Item> 
              {this.state.numberError ? 
                <Text style={{marginTop: -15, paddingLeft: 20, color: 'red', fontSize: 15, marginBottom: 1}}>Please Enter Email</Text>
                :<Text></Text>
              } 
                          
              <Item rounded style={[{marginBottom: 15},commonstyle.borderstyle]}>
               <Icon style={[{ left: 20},commonstyle.iconstyle]}
                      size={this.props.iconSize}
                      name="lock-outline"
                />
                <Input secureTextEntry={this.state.securepassword}
                    placeholderTextColor ='white'
                    placeholder="Password"
                    style={commonstyle.loginputstyle}
                    returnKeyType={"done"}
                    onChange={this.passwordTextChanged}
                />
                <Icon style={[{ right: 20},commonstyle.iconstyle]}
                      name={this.state.icEye}
                      size={this.props.iconSize}
                      onPress={this.changePwdType}
                />
              </Item>
              {this.state.passwordError ? 
                <Text style={{marginTop: -15, paddingLeft: 20, color: 'red', fontSize: 15, marginBottom: 1}}>Please Enter Password</Text>
                :<Text></Text>
              } 
              <Button rounded block style={[{shadowOffset: {height: 0, width: 0},
    shadowOpacity: 0},commonstyle.buttonbackgroundcolor]} 
		      		onPress={this.loginPressed} >
                <Text   uppercase={false} style={commonstyle.buttoncolor}>Login</Text>
              </Button>
            </Form>
          </View>          
          <View style={{ paddingRight: 70, paddingLeft: 70, justifyContent: 'center', alignItems: 'center',marginTop:8}}>
            <Text style={{color:'white'}} onPress={() => { this.setForgePasstModalVisible(true) }}>Forgot Password?</Text>
          </View>
          {/* <Footer transparent style={{ marginTop:height-520,
           
            }}  noShadow  >
                    <Text style={{color:'black',fontSize:10}}>Powered by Dexbytes Infotech PVT LTD</Text>
          </Footer> */}
          <Text style={{marginTop:height-528,textAlign:'center',color:'white'}} >Powered by One Key Solution</Text>
        </Content>
        </ImageBackground>
        <View >
              <Modal
                animationType="slide"
                transparent={false}
                
                visible={this.state.ForgetPassmodalVisible}
                onRequestClose={() => {}}>
                <Header style={{backgroundColor:'#5b6567'}}  noShadow >
                    <StatusBar  style={commonstyle.headerbackgroundcolor} />
                    <Left>
                        <Button transparent onPress={() => this.setForgePasstModalVisible(!this.state.ForgetPassmodalVisible)}>
                        <Icon style={{color:'white'}} name="arrow-back" size={this.props.iconSize} />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={[{width:width/1.5},commonstyle.headertitlecolor]}>Forget Password</Title>
                    </Body>
                    <Right>
                        <Text style={{ fontSize:17,color: 'white' }}></Text>
                    </Right>
                </Header>
                <Content style={{ backgroundColor:'#5b6567'}} padder>
                  <View  style={{ marginTop: 40 }}>
                    {/* <H2 style={{ marginBottom: 20 }}>Forget Password</H2> */}
                    {this.state.verifyOtpstatus == false ?
                    
                    <View>
                      <Text style={{color:'white',textAlign:'center',marginBottom:25, marginLeft: 40, marginRight: 40}}>Enter your registerd mobile number to send otp for verification</Text>
                        <Form>
                        <Item rounded  style={[{marginBottom: 15},commonstyle.borderstyle]}>
                            <FontAwesome style={[{ left: 25},commonstyle.iconstyle]}
                                size={this.props.iconSize}
                                color={this.props.iconColor}
                                name="mobile-phone"
                              />
                            <Input keyboardType='numeric'
                                placeholderTextColor='white'
                                placeholder="Mobile number"
                                maxLength={10}
                                style={commonstyle.loginputstyle}
                                value={this.state.email}
                                returnKeyType={"done"}
                                onChange={this.mobileNumebrChanged}
                            />
                      </Item>
                
                      <Button  onPress={this.sendOtp} rounded  block style={commonstyle.buttonbackgroundcolor}>
                        <Text uppercase={false} style={commonstyle.buttoncolor}>Submit</Text>
                      </Button>
                    </Form>
                  
                  <View style={{ paddingRight: 70, paddingLeft: 70, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={{marginBottom: 10,marginTop:10, color:'white' }}>Wait for the sms with code</Text>
                    <Text onPress={this.sendOtp} style={{color:'#40d8e8'}}>Send again?</Text>
                  </View>
                  </View>
                  : null}
                  {this.state.sendOtpstatus ?
                  <View style={{ padding: 20}}>
                    <Form>
                      <Item rounded  style={[{marginBottom: 15},commonstyle.borderstyle]}>
                          <Input keyboardType='numeric'
                                placeholderTextColor='white'
                                maxLength={4}
                                placeholder="OTP number"
                                style={commonstyle.loginputstyle}
                                value={this.state.otpno}
                                returnKeyType={"done"}
                                onChange={this.otpTextChanged}
                          />
                        </Item>
                     
                      <Button  onPress={this.VerifyOtp} rounded  block style={commonstyle.buttonbackgroundcolor}>
                        <Text uppercase={false} style={commonstyle.buttoncolor}>Verify OTP</Text>
                      </Button>
                    </Form>
                  </View>
                  : null}
                  {this.state.verifyOtpstatus ?
                  <View>
                      <Form>
                          <Item rounded  style={[{marginBottom: 15},commonstyle.borderstyle]}>
                          <Icon style={[{ left: 20},commonstyle.iconstyle]}
                                  size={this.props.iconSize}
                                  color={this.props.iconColor}
                                  name="lock-outline"
                            />
                            <Input secureTextEntry={this.state.securepassword}
                                placeholder="Password"
                                placeholderTextColor='white'
                                style={commonstyle.loginputstyle}
                                returnKeyType={"done"}
                                value={this.state.newpassword}
                                onChange={this.newpasswordTextChanged}
                            />
                            <Icon style={[{ right: 20},commonstyle.iconstyle]}
                                  name={this.state.pEye}
                                  size={this.props.iconSize}
                                  color={this.props.iconColor}
                                  onPress={this.setPwd}
                            />
                          </Item>              
                          <Item rounded  style={[{marginBottom: 15},commonstyle.borderstyle]}>
                          <Icon style={[{ left: 20},commonstyle.iconstyle]}
                                  size={this.props.iconSize}
                                  color={this.props.iconColor}
                                  name="lock-outline"
                            />
                            <Input secureTextEntry={this.state.secureconfpassword}
                                placeholder="Confirm Password"
                                placeholderTextColor='white'
                                style={commonstyle.loginputstyle}
                                returnKeyType={"done"}
                                value={this.state.conpassword}
                                onChange={this.conPasswordtextChanged}
                            />
                            <Icon style={[{ right: 20},commonstyle.iconstyle]}
                                  name={this.state.cpEye}
                                  size={this.props.iconSize}
                                  color={this.props.iconColor}
                                  onPress={this.setConfPwd}
                            />
                          </Item>
                            <Button  onPress={this.saveForgetPassword} rounded  block style={commonstyle.buttonbackgroundcolor}>
                              <Text uppercase={false} style={commonstyle.buttoncolor}>Submit</Text>
                            </Button>
                        </Form>
                    </View> 
                     : null} 
                  </View>  
                </Content>
              </Modal>
            </View>
            {/* loader model */}
            <Modal
              transparent={true}
              animationType={'none'}
              visible={this.state.isLoading}
              onRequestClose={() => {}}>
              <View style={commonstyle.modalBackground}>
                <View style={commonstyle.activityIndicatorWrapper}>
                  <ActivityIndicator
                    animating={this.state.isLoading} size="large" color="black" />
                </View>
              </View>
            </Modal>
      </Container>
    );
  }
}

Login.defaultProps = {
  iconSize:25,
}