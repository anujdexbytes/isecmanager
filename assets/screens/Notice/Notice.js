import React, { Component } from 'react';
import { Dimensions,} from 'react-native';
import { Button, Container, Header, Text, Left, Content,TabHeading, Body, Right,Title, Icon,  } from "native-base";
const window = Dimensions.get("window");
export default class Noticescreen extends Component { 
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = { tab:null,
                   screeHW: Dimensions.get('window')
                };
  }
  componentWillReceiveProps(nextprop){
    console.log(nextprop.navigation.state.routeName)
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Header style={{ marginTop:window.height*0.028}}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack()} >
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={{width:200}} >Notice Board</Title>
          </Body>
          <Right />
        </Header>
     
     </Container>
    );
  }
}