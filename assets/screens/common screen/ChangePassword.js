import React, { Component } from 'react';
import { Dimensions, StatusBar, TouchableOpacity, ImageBackground} from 'react-native'; 
import { Button, Container, Header, Text, View, Left, Content, Body, Right,Title, Icon,Form, Item, Input, } from "native-base";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import commonstyle from "../../commonstyle";
import WebServiceHandler from "../../webservice";

const window = Dimensions.get("window");

export default class ChangePassword extends Component { 
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {  tab:null,
                    screeHW: Dimensions.get('window'),
                    pEye: 'visibility-off',
                    cpEye: 'visibility-off',
                    curEye:'visibility-off',
                    securepassword: true,
                    secureconfpassword: true,
                    securecurrpassword: true,
                    currentpassword:'',
                    password: '',
                    conpassword: '',
                };
  }
//=============================================/
//        Change password visibility          //
//=============================================/
currpaassPwdType = () => {
    let currpassState;
    if (this.state.securecurrpassword) {
        currpassState = {
            curEye: 'visibility',
            securecurrpassword: false
        }
    } else {
        currpassState = {
            curEye: 'visibility-off',
            securecurrpassword: true
        }
    }
   // set new state value
    this.setState(currpassState)
  };
  
  setPwd = () => {
  let newPassState;
  if (this.state.securepassword) {
    newPassState = {
        pEye: 'visibility',
        securepassword: false
      }
  } else {
    newPassState = {
        pEye: 'visibility-off',
        securepassword: true
      }
  }
   // set new password state value
   this.setState(newPassState)
  };
  
  setConfPwd = () => {
  let newConfPassState;
  if (this.state.secureconfpassword) {
      newConfPassState = {
          cpEye: 'visibility',
          secureconfpassword: false
      }
  } else {
      newConfPassState = {
          cpEye: 'visibility-off',
          secureconfpassword: true
      }
  }
  // set new newConfPassState state value
  this.setState(newConfPassState)
  }; 
  
//=============================================/
//      Change  password  functionallity      //
//=============================================/
  currentpasswordTextChanged = (event) => {
    this.setState({ currentpassword: event.nativeEvent.text })
  } 

  newpasswordTextChanged = (event) => {
    this.setState({ newpassword: event.nativeEvent.text })
  } 
  
  conPasswordtextChanged = (event) => {
    this.setState({ conpassword: event.nativeEvent.text })
  } 
//====================================================/
//  function for Change password (saveChangepassword) //
//====================================================/
saveChangepassword = () => {
  if (this.state.currentpassword == '') {
    alert('Please enter current password')  
  }else if (this.state.newpassword == '') {
    alert('Please enter  password')  
  }else if (this.state.conpassword == '') {
    alert('Please enter confirm password');
  }else if (this.state.newpassword != this.state.conpassword) {
    alert('Please enter confirm password same as password');
  } else {
    this.setState({ isLoading: true });
       let data = JSON.stringify({
          user_id: this.state.user_data.id,
          current_password:this.state.currentpassword,
          password: this.state.newpassword
        });
        WebServiceHandler.saveChangepassword(data)
        .then(response => {   
        let status = response.status;
        console.log('change password response', response)
        if (status == true) {   
          this.setState({ isLoading: false,
                          visibleModal:false
                        });
          alert('Your password has been changed successfully!');
        } else {              
          this.setState({ isLoading: false });        
          let msg = response.message;
          console.log('message', msg)
          alert(msg)
        }
      }).catch(error => {
        this.setState({ isLoading: false });
        alert(error)
      });       
  }
}
componentWillReceiveProps(nextprop){
  console.log(nextprop.navigation.state.routeName)
}
  render() {
    const { navigate } = this.props.navigation;
    return (
    
      <Container>
          <Header style={{ marginTop:window.height*0.028}}>
            <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack()} >
              <Icon name="arrow-back" />
            </Button>
            </Left>
            <Body>
              <Title style={{width:200}} >Change Password</Title>
            </Body>
            <Right />
          </Header>
          <Content padder>
                <View> 
                    <Form style={{marginBottom: 25}}>
                        <Item rounded style={{marginBottom: 15}}>
                            <MaterialIcons style={[{ left: 15},commonstyle.changepassiconstyle]}
                                  size={25}
                                  name="lock-outline"
                              />
                              <Input secureTextEntry={this.state.securecurrpassword}
                                  placeholder="Current Password"
                                  style={{marginLeft: 35, marginRight: 20}}
                                  returnKeyType={"done"}
                                  value={this.state.currentpassword}
                                  onChange={this.currentpasswordTextChanged}
                              />
                            
                              <MaterialIcons style={[{ right: 20},commonstyle.changepassiconstyle]}
                                  name={this.state.curEye}
                                  size={25}
                                  onPress={this.currpaassPwdType}
                              />
                        </Item>   
                        <Item rounded style={{marginBottom: 15}}>
                            <MaterialIcons style={[{ left: 15},commonstyle.changepassiconstyle]}
                                  size={25}
                                  name="lock-outline"
                              />
                              <Input secureTextEntry={this.state.securepassword}
                                  placeholder="Password"
                                  style={{marginLeft: 35, marginRight: 20}}
                                  returnKeyType={"done"}
                                  value={this.state.newpassword}
                                  onChange={this.newpasswordTextChanged}
                              />
                              
                              <MaterialIcons style={[{ right: 20},commonstyle.changepassiconstyle]}
                                  name={this.state.pEye}
                                  size={25}
                                  onPress={this.setPwd}
                              />
                        </Item>              
                        <Item rounded style={{marginBottom: 15}}>
                            <MaterialIcons style={[{ left: 15},commonstyle.changepassiconstyle]}
                                  size={25}
                                  name="lock-outline"
                              />
                              <Input secureTextEntry={this.state.secureconfpassword}
                                  placeholder="Confirm Password"
                                  style={{marginLeft: 35, marginRight: 20}}
                                  returnKeyType={"done"}
                                  value={this.state.conpassword}
                                  onChange={this.conPasswordtextChanged}
                              />
                              <MaterialIcons style={[{ right: 20},commonstyle.changepassiconstyle]}
                                name={this.state.cpEye}
                                size={25}
                                onPress={this.setConfPwd}
                                />
                        </Item>
                        <Button rounded block  
                                    onPress={this.saveChangepassword}>
                               <Text   uppercase={false} style={{fontSize: 20}}>Submit</Text>
                        </Button>
                    </Form>
                </View>
          </Content>
      </Container>
    
    );
  }
}