import React, { Component } from 'react';
import { Dimensions, StatusBar, TouchableOpacity, ImageBackground} from 'react-native'; 
import { Button, Container, Header, Text, View, Left, Content,TabHeading, Body, Right,Tab, Tabs, Title, Icon, H2, Form, Item, Input, Picker } from "native-base";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import commonstyle from "../../commonstyle"

const window = Dimensions.get("window");

export default class TermsConditions extends Component { 
  static navigationOptions = {
    header: null
  };
  componentWillReceiveProps(nextprop){
    console.log(nextprop.navigation.state.routeName)
  }
  constructor(props) {
    super(props);
    this.state = { tab:null,
                   screeHW: Dimensions.get('window')
                };
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
    
      <Container  >
        <Header style={{ marginTop:window.height*0.028}}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack()} >
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={{width:200}} >Terms and Conditions</Title>
          </Body>
          <Right />
        </Header>
          <Content padder >
              <View > 
                 <Text>Terms and Conditions</Text>
              </View>
          </Content>
      </Container>
    
    );
  }
}