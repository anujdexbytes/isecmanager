import React, { Component } from 'react';
import { Dimensions, StatusBar, ActivityIndicator, TouchableOpacity, Modal, Image} from 'react-native'; 
import { Button, Container, Header, View, Left, Content, Body, Right, Title, Icon,Text, ActionSheet, H2, Form, Item, Input, Picker,Label } from "native-base";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import commonstyle from "../../commonstyle";
import userDefaults from 'react-native-default-preference'
import WebServiceHandler  from '../../webservice';
import ImagePicker from 'react-native-image-crop-picker';
import constant from "../../constatant" ;
const window = Dimensions.get("window");
var BUTTONS = [
  { text: 'Open camera',value:1,iconColor: "black", icon: "camera" },
  {text: 'Open Gallery',value:2, iconColor: "black",icon: "md-photos" },
  { text: "Cancel", icon: "close", iconColor: "black" }
];
var CANCEL_INDEX = 2;
export default class EditProfile extends Component { 
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = { tab:null,
                   screeHW: Dimensions.get('window'),
                   user_data:'',
                   userDetail:{},
                   fullName:'',
                   nickName:'',
                   userEmail:'',
                   userPhone:'',
                   image:null, 
                   isLoading:false
                };
  }

  getUserdetail = () => {
    WebServiceHandler.getUserdetail(this.state.user_data.customer_id)
    .then(response => { 
    if (response.success) { 
       this.setState({
        userDetail:response.success.data,
        fullName: response.success.data.civilit + ' ' + response.success.data.firstname + ' ' + response.success.data.lastname,
        nickName:response.success.data.nickname,
        userEmail:response.success.data.email,
        userPhone:response.success.data.mobile,
        image:response.success.data.image
       })
      
    } else {                      
      let msg = response.msg;
      alert(msg)
    }
    }).catch(error => {
      alert(error)
  });       
  } 

  getUserprofile =() =>{
    userDefaults.get('user_data').then((value) => {
      this.setState({ user_data : JSON.parse(value)})
      this.getUserdetail();
    });
  }

  pickSingleWithCamera(cropping) {
    ImagePicker.openCamera({
      width: 130,
      height: 130,
      cropping: cropping,
      includeBase64: true,
      includeExif: true,
      compressImageMaxWidth:300,
      compressImageMaxHeight:200
    }).then(image => {
      this.setState(
        {
          image :  `data:${image.mime};base64,`+ image.data, width: image.width, height: image.height,
        }
      );
      console.log("asfasfaffasfasfaf",this.state.image);
    }).catch(e => console.log(e));
  }

  pickGallery() {
    ImagePicker.openPicker({
      waitAnimationEnd: false,
      includeExif: true,
      includeBase64: true,
      mediaType:"photo",
      width:130,
      height:130,
      selected1:"null",
      compressImageMaxWidth:300,
      compressImageMaxHeight:200
    }).then(image => {
      this.setState(
        {
          image :  `data:${image.mime};base64,`+ image.data, width: image.width, height: image.height,
      }
    );
    }).catch(e => console.log(e));
  }

  componentWillMount(){
    this.getUserprofile()
  } 

  renderImage= (image) => {
    console.log(image);
    return <Image style={{width:130, height:130,borderColor :'grey',borderRadius:65, borderWidth:3}} source={{uri:image}} />
  }
  // componentWillReceiveProps(nextprop){
  //   console.log(nextprop.navigation.state.routeName)
  // }

  //========================================================/
  //  function for update User profile (updateUserprofile)  //
  //========================================================/
  nickNameTextChanged = (event) => {
    this.setState({ nickName: event.nativeEvent.text })
  }

  updateUserprofile = () => {
    let data;
    if (this.state.nickName == '') {
      alert('Enter your nick Name');
    }else {
      if(this.state.image != null){
        data = JSON.stringify({
         image: this.state.image,
         nickname: this.state.nickName,
         pId: constant.PROPERTY_ID
       });  
     }else{
        data = JSON.stringify({
         nickname: this.state.nickName,
         pId: constant.PROPERTY_ID
       });  
     }
      this.setState({ isLoading: true });
          WebServiceHandler.updateUserprofile(data,this.state.user_data.customer_id)
          .then(response => {    
          if (response.success) { 
            this.setState({ isLoading: false });
            alert("Profile updated sucessfully!");
            this.getUserdetail();
          } else {   
            this.setState({ isLoading: false });                   
            let msg = response.message;
            alert(msg)
          }
        }).catch(error => {
          this.setState({ isLoading: false });
          alert(error)
        });    
    }
  }

  

  render() {
    const { navigate } = this.props.navigation;
    return (
    
      <Container  >
        <Header style={{ marginTop:window.height*0.028}}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack()} >
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={{width:200}} >Edit Profile</Title>
          </Body>
          <Right>
            <Button
              transparent
              onPress={() => this.updateUserprofile()} >
             <Text>Save</Text>
            </Button>
          </Right>  
        </Header>
        <Content style={{padding:7}}>
          <View style={{ marginBottom: 20 }}>
            <View>
              <View style={{ marginBottom: 10, flexDirection: 'row', justifyContent: 'center' }}>
              {this.state.image != null ? [this.renderImage(this.state.image)] 
               : <Image source = {{uri:'http://liveinloops.net/app/backoffice/public/default_avatar.jpg'}} style =  {{ width:130, height:130,borderColor :'grey',borderRadius:65, borderWidth:3}} /> 
              }
              </View>
                <Text onPress={() => ActionSheet.show({
                                                          options: BUTTONS,
                                                          cancelButtonIndex: CANCEL_INDEX,
                                                          title: "Select Option"
                                                        },
                                                        buttonIndex => {
                                                          if(BUTTONS[buttonIndex].value == 1){
                                                            this.pickSingleWithCamera();
                                                          }else if(BUTTONS[buttonIndex].value ==2){
                                                            this.pickGallery();
                                                          }
                                                        }
                                                        )} 
                                                        style={{textAlign:'center'}}>
                                                        Change Photo
                </Text>
              </View>
             <View>
                <Form>
                  <Item stackedLabel>
                    <Label>Nick Name</Label>
                    <Input  value={this.state.nickName} 
                            returnKeyType={"done"}
                            onChange={this.nickNameTextChanged} />
                  </Item>
                  <Item stackedLabel>
                    <Label>Full Name</Label>
                    <Input disabled value={this.state.fullName} />
                  </Item>
                  <Item stackedLabel >
                    <Label>Mobile Number</Label>
                    <Input disabled value={this.state.userPhone} />
                  </Item>
                  <Item stackedLabel >
                    <Label>Email</Label>
                    <Input disabled value={this.state.userEmail} />
                  </Item>
                  <Item stackedLabel >
                    <Label>Password</Label>
                    <Input disabled value="**********" />
                  </Item>
                  
                </Form>
             </View>     
           </View> 
          </Content>
           {/* loader model */}
           <Modal
              transparent={true}
              animationType={'none'}
              visible={this.state.isLoading}
              onRequestClose={() => {}}>
              <View style={commonstyle.modalBackground}>
                <View style={commonstyle.activityIndicatorWrapper}>
                  <ActivityIndicator
                    animating={this.state.isLoading} size="large" color="black" />
                </View>
              </View>
            </Modal>
      </Container>
    
    );
  }
}