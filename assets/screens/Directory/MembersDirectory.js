import React, { Component } from 'react';
import {  Dimensions, AsyncStorage, Platform, Alert, ActivityIndicator, StatusBar,Modal ,View} from 'react-native';
import { Button, Container, Header, Content, Label,List, ListItem, Fab, Icon, Text, Textarea, Thumbnail, Left, Body, Right, Title} from "native-base";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Entypo from "react-native-vector-icons/Entypo";
import commonstyle from "../../commonstyle"
export default class MembersDirectory extends Component {
  static navigationOptions = {
    header: null
  };
  
  constructor(props) {
    super(props);
    console.log("props",props);
    this.state = {
      screeHW: Dimensions.get('window'),
      visibleModal:false,
      btnSelected: 1
    }
  }
//=============================================/
//     Add new complaint modal show hide      //
//=============================================/
  ModalVisible = (value) =>{
      this.setState({visibleModal: value});
  }
  selectComplainttype = (value) =>{
    this.setState({btnSelected: value});
  }
  
  render() {
    let {width, height} = this.state.screeHW;
    var items = [
      'Simon Mignolet',
      'Nathaniel Clyne',
      'Dejan Lovren',
      'Mama Sakho',
      'Emre Can'
    ];
    return (
      <Container>
        <Content>
          <List dataArray={items}
            renderRow={(item) =>
              <ListItem avatar>
              <Left>
                <Thumbnail source={{ uri: 'http://s3.amazonaws.com/37assets/svn/765-default-avatar.png' }} />
              </Left>
              <Body>
                <Text>{item}</Text>
                <View style={{flexDirection:'row',}}>
                <Text>B-101</Text>  
                <FontAwesome style={{marginHorizontal:30}} size={25} name="phone" />
                <Entypo style={{marginHorizontal:30}} size={25} name="chat" />
                </View>
              </Body>
              {/* <Right>
                <Text note>B-101</Text>
              </Right> */}
            </ListItem>
            }>
          </List>
          
        </Content>
      </Container>
    );
  }
}