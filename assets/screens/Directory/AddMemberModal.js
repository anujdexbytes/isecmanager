import React, { Component } from 'react';

import { Container, Header, Fab, Content, List, ListItem,  Left, Body, Right, Form, Thumbnail, Text, View, Icon, Title, Item, Input, Button } from 'native-base';
import {Modal, TextInput, Dimensions, AppRegistry, StyleSheet} from "react-native";
import commonstyle from "../../commonstyle"

let {width, height} = Dimensions.get('window');

export default class AddMemberModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleMemberModal:false,
      mob_number: '',
      email: ''
    }
  }
  render() {
    return (
              <View>
                <Form>
                  <ListItem avatar style={{marginBottom: 15}}>
                    <Left>
                      <Thumbnail style={{height: 50, width: 50}} source={{ uri: 'http://liveinloops.net/app/backoffice/public/default_avatar.jpg' }} />
                    </Left>
                    <Body style={{borderBottomWidth: 0}}>
                      <TextInput
                        style={{height: 40}}
                        placeholder="Name"
                        onChangeText={(text) => this.setState({name:text})}
                      />
                    </Body>
                  </ListItem>
                  <Item style={{marginBottom: 15, borderBottomWidth: 0}}>
                    <TextInput
                      style={{width: width}}
                      placeholder="Flat No."
                      onChangeText={(text) => this.setState({flat_no:text})}
                    />
                  </Item>
                  <Item style={{marginBottom: 15, borderBottomWidth: 0}}>
                    <TextInput
                      keyboardType='numeric'
                      style={{width: width}}
                      placeholder="Mobile No."
                      onChangeText={(text) => this.setState({number:text})}
                    />
                  </Item>
                  <Item style={{marginBottom: 15, borderBottomWidth: 0}}>
                    <TextInput
                      keyboardType='email-address'
                      style={{width: width}}
                      placeholder="Email"
                      onChangeText={(text) => this.setState({email:text})}
                    />
                  </Item>
                  {this.props.tab.type == 'handiman' ?
                    <Item style={{marginBottom: 15, borderBottomWidth: 0}}>
                      <TextInput
                        keyboardType='email-address'
                        style={{width: width}}
                        placeholder="Email"
                        onChangeText={(text) => this.setState({email:text})}
                      />
                    </Item>
                  :null
                  }
                  <Button rounded block style={[{shadowOffset: {height: 0, width: 0},
                    shadowOpacity: 0},commonstyle.buttonbackgroundcolor]} 
		      		      onPress={this.submitPressed} >
                    <Text uppercase={false} style={commonstyle.buttoncolor}>Submit</Text>
                  </Button>
                </Form>
              </View>
    );
  }
}

AppRegistry.registerComponent('AddMemberModal', () => AddMemberModal);