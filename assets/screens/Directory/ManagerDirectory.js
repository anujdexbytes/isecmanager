import React, { Component } from 'react';
import { Container, Header, Fab, Content, List, ListItem,  Left, Body, Right, Form, Thumbnail, Text, View, Icon, Title, Item, Input, Button } from 'native-base';
import {Modal, TextInput, Dimensions, AppRegistry, StyleSheet} from "react-native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Entypo from "react-native-vector-icons/Entypo";
import commonstyle from "../../commonstyle"
import AddMemberModal from './AddMemberModal';

let {width, height} = Dimensions.get('window');

export default class ManagerDirectory extends Component {

  constructor(props) {
    super(props);
    this.state = {
      visibleMemberModal:false,
      mob_number: '',
      email: ''
    }
  }

  addMemberModalVisible = (value) =>{
    this.setState({visibleMemberModal: value});
  }

  render() {
    var items = [
      'Simon Mignolet',
      'Nathaniel Clyne',
      'Dejan Lovren',
      'Mama Sakho',
      'Emre Can'
    ];
    return (
      <Container>
        <Content>
          <List dataArray={items}
            renderRow={(item) =>
              <ListItem avatar>
              <Left>
                <Thumbnail source={{ uri: 'http://s3.amazonaws.com/37assets/svn/765-default-avatar.png' }} />
              </Left>
              <Body>
                <Text>{item}</Text>
                <Text>9098553398</Text>  
              </Body>
              <Right>
                <Text style={{color:"red"}} >Maintanance</Text>
                <View style={{flexDirection:'row'}}>
                <Left>
                <FontAwesome  size={21} name="phone" />
                </Left>
                <Entypo style={{}} size={21} name="chat" />
                </View>
              </Right>
            </ListItem>
            }>
          </List>
        </Content>
          
        {this.state.visibleMemberModal ?
        <View >
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.visibleMemberModal}
          onRequestClose={() => {}}>
          <Header>
            <Left>
              <Button
                transparent
                onPress={() => this.addMemberModalVisible(!this.state.visibleMemberModal)} >
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body>
              <Title style={{width:200}} >Add Management</Title>
            </Body>
            <Right />
          </Header>
          <Content padder>
    
          <AddMemberModal />

          
          </Content>
            </Modal>
        </View>     
          :null
        }

        {/* <Fab
          active="true"
          containerStyle={{ }}
          style={{ backgroundColor: 'blue' }}
          position="bottomRight"
          onPress={() => this.addMemberModalVisible(!this.state.visibleMemberModal)}>
          <MaterialIcons name="add" />
        </Fab> */}
      </Container>
    );
  }
}

AppRegistry.registerComponent('ManagerDirectory', () => ManagerDirectory);