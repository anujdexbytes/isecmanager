import React, { Component } from 'react';
import {Modal, StatusBar,AsyncStorage,ActivityIndicator,View, AppRegistry } from 'react-native';
import { Container, Header, Content, List,Fab, ListItem, Left, Body, Right, Thumbnail, Text,Icon, Button, Title } from 'native-base';
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Entypo from "react-native-vector-icons/Entypo";
import commonstyle from "../../commonstyle";
import AddMemberModal from './AddMemberModal';

export default class HandimanDirectory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visibleMemberModal:false,
      mob_number: '',
      email: ''
    }
  }

  addMemberModalVisible = (value) =>{
    this.setState({visibleMemberModal: value});
  }
  render() {
    var items = [
      'Simon Mignolet',
      'Nathaniel Clyne',
      'Dejan Lovren',
      'Mama Sakho',
      'Emre Can',
      'Simon Mignolet',
      'Nathaniel Clyne',
      'Dejan Lovren',
      'Mama Sakho',
      'Emre Can',
      'Simon Mignolet',
      'Nathaniel Clyne',
      'Dejan Lovren',
      'Mama Sakho',
      'Emre Can'
    ];
    return (
      <Container>
        <Content>
          <List dataArray={items}
            renderRow={(item) =>
              <ListItem avatar>
              <Left>
                <Thumbnail source={{ uri: 'http://s3.amazonaws.com/37assets/svn/765-default-avatar.png' }} />
              </Left>
              <Body>
                <Text>{item}</Text>
                <View style={{flexDirection:'row',marginTop:3}}>
                <Text>9098553398</Text>  
                <FontAwesome style={{marginHorizontal:30}} size={25} name="phone" />
                </View>
              </Body>
              <Right>
                <Text style={{color:"red",marginTop:3}} >Plumber</Text>
                <Entypo style={{marginHorizontal:30}} size={25} name="chat" />
              </Right>
            </ListItem>
            }>
          </List>
        </Content>

        {this.state.visibleMemberModal ?
        <View >
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.visibleMemberModal}
          onRequestClose={() => {}}>
          <Header>
            <Left>
              <Button
                transparent
                onPress={() => this.addMemberModalVisible(!this.state.visibleMemberModal)} >
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body>
              <Title style={{width:200}} >Add Handyman</Title>
            </Body>
            <Right />
          </Header>
          <Content padder>
    
          <AddMemberModal  tab={{type:'handiman'}} />

          
          </Content>
            </Modal>
        </View>     
          :null
        }

        <Fab
          active="true"
          containerStyle={{ }}
          style={{ backgroundColor: 'blue' }}
          position="bottomRight"
          onPress={() => this.addMemberModalVisible(!this.state.visibleMemberModal)}>
          <MaterialIcons name="add" />
        </Fab>
      </Container>
    );
  }
}

AppRegistry.registerComponent('HandimanDirectory', () => HandimanDirectory);