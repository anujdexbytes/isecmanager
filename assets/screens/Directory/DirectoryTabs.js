import React, { Component } from 'react';
import { Dimensions, StatusBar, TouchableOpacity, ImageBackground} from 'react-native';
import { Button, Container, Header, Text, Left, Content,TabHeading, Body, Right,Tab, Tabs, Title, Icon, H2, Form, Item, Input, Picker } from "native-base";
import ManagerDirectory from "./ManagerDirectory";
import HandimanDirectory from "./HandimanDirectory";
import MembersDirectory from "./MembersDirectory";
const window = Dimensions.get("window");
export default class ComplaintTab extends Component { 
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = { tab:null,
                   screeHW: Dimensions.get('window')
                };
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Header>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack()} >
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={{width:200}} >Directory</Title>
          </Body>
          <Right />
        </Header>
       <Tabs  onChangeTab={({ i, ref }) => this.setState({tab : i}) } locked={true} initialPage={0}  tabBarUnderlineStyle={{backgroundColor:'#191970',borderBottomWidth:0}} >
          <Tab  heading="Management"   activeTextStyle={{color: 'black', fontWeight: 'bold'}} textStyle={{color: 'black'}} activeTabStyle={{backgroundColor: '#d4d7ea'}} tabStyle={{backgroundColor: '#d4d7ea'}} >
            <ManagerDirectory tabProps={{screen:this.state.tab}} />
          </Tab>
          <Tab heading="Handiman"  activeTextStyle={{color: 'black', fontWeight: 'bold'}}  textStyle={{color: 'black'}} activeTabStyle={{backgroundColor: '#d4d7ea'}} tabStyle={{backgroundColor: '#d4d7ea'}}>
            <HandimanDirectory  tabProps={{screen:this.state.tab}} />
          </Tab>
          <Tab heading="Members"  activeTextStyle={{color: 'black', fontWeight: 'bold'}} textStyle={{color: 'black'}} activeTabStyle={{backgroundColor: '#d4d7ea'}} tabStyle={{backgroundColor: '#d4d7ea'}}>
            <MembersDirectory  tabProps={{screen:this.state.tab}} />
          </Tab>
        </Tabs>
     </Container>
    );
  }
}