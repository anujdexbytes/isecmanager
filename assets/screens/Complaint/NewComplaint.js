import React, { Component } from 'react';
import {  Dimensions, AsyncStorage, ScrollView, Alert,ListView, FlatList, Image, TouchableOpacity , ActivityIndicator, Modal ,View} from 'react-native';
import { Button, Container, Header, Content, Label, List, Card,Item,Input, CardItem, Thumbnail,Footer, ListItem, ActionSheet, Fab, Icon, Text, Textarea,  Left, Body, Right, Title,Picker} from "native-base";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import EvilIcons from "react-native-vector-icons/EvilIcons";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Foundation from "react-native-vector-icons/Foundation";
import commonstyle from "../../commonstyle"
import ImagePicker from 'react-native-image-crop-picker';
import RadioButton from 'radio-button-react-native';
import userDefaults from 'react-native-default-preference';
import constant from "../../constatant" ;
import WebServiceHandler from "../../webservice";
import Moment from 'moment';

var BUTTONS = [
  { text: 'Open camera',value:1,iconColor: "black", icon: "camera" },
  {text: 'Open Gallery',value:2, iconColor: "black",icon: "md-photos" },
  { text: "Cancel", icon: "close", iconColor: "black" }
];
var CANCEL_INDEX = 2;
let {width, height} = Dimensions.get('window');
export default class NewComplaint extends Component {
  static navigationOptions = {
    header: null
  };
  
  constructor(props) {
    super(props);
    this.state = {
      isLoading:true,
      visibleModal:false,
      customer_id:null,
      visibleCommentModal:false,
      visibleDetailModal:false,
      image:{uri:null} ,
      images: null,
      value: 0,
      discription:null,
      user_data:{},
      customer_unit_id:null,
      Complaintlist:[],
      Commentlist:[],
      message:null,
      comment_discription:'',
      likeModal:false,
      likeList:[],
      complaintDetail:'',
      status: ''
    }
  }

  //===================================================/
  // function for show like list  (getLikelist)      //
  //==================================================/
  getLikelist = (cID) => {
    userDefaults.get('customer_id').then((value) => {
    this.setState({ customer_id : JSON.parse(value)})
      let data = JSON.stringify({
                  cId: cID,
                  type:0
                });  
        this.setState({ isLoading: true }); 
        WebServiceHandler.getCommentList(data)
        .then(response => {    
        console.log(response);
        if (response.success) { 
          this.setState({ isLoading: false ,
            likeList:response.success.data,
            message:null});
        } else {   
          this.setState({ message :response.msg,
            isLoading: false });                   
          let msg = response.msg;
        }
      }).catch(error => {
        this.setState({ isLoading: false });
        alert(error)
      }); 
    });   
  };
 
  //===================================================/
  // function for show comment list  (getCommentlist)  //
  //====================================================/
  getCommentlist = (cID) => {
    userDefaults.get('customer_id').then((value) => {
    this.setState({ customer_id : JSON.parse(value)})
      let data = JSON.stringify({
                  cId: cID,
                  type:1
                });  
        this.setState({ isLoading: true }); 
        WebServiceHandler.getCommentList(data)
        .then(response => {    
        console.log(response);
        if (response.success) { 
          this.setState({ isLoading: false ,
            Commentlist:response.success.data,
            message:null});
        } else {   
          this.setState({ message :response.msg,
            isLoading: false });                   
          let msg = response.msg;
        }
      }).catch(error => {
        this.setState({ isLoading: false });
        alert(error)
      }); 
    });   
  };
  //=====================================================/
  // function for show Complaint list (getComplaintlist) //
  //=====================================================/
  getComplaintlist = () => {
      userDefaults.get('customer_id').then((value) => {
        this.setState({ customer_id : JSON.parse(value)})
        let data = JSON.stringify({
                    cId: this.state.customer_id,
                    pId: constant.PROPERTY_ID,
                    status:1,
                  });  

      this.setState({ isLoading: true });

      WebServiceHandler.getComplaintlist(data).then(response => {
        if (response.success) { 
          this.setState({ isLoading: false ,
            Complaintlist:response.success.data});
        } else {   
          this.setState({ isLoading: false });                   
          let msg = response.message;
          alert(msg)
        }
      }).catch(error => {
        this.setState({ isLoading: false });
        alert(error)
      }); 
    });   
  };

  
  getUserdetail =() =>{
    this.getComplaintlist();
    userDefaults.get('user_data').then((value) => {
      this.setState({ user_data : JSON.parse(value)})
    });
    userDefaults.get('customer_unit_id').then((value) => {
      console.log("customer_unit_id",value);
      this.setState({ customer_unit_id : JSON.parse(value)})
    });
  }

  componentWillMount(){
    this.getComplaintlist();
  } 

  handleOnPress(value){
    this.setState({value:value})
  }
  //===========================================================/
  // call when props or tab change (componentWillReceiveProps)//
  //===========================================================/
  componentWillReceiveProps(nextProps) {  
    this.getComplaintlist();
}
//=============================================/
//     Add new complaint modal show hide      //
//=============================================/
  ModalVisible = (value) =>{
      this.setState({visibleModal: value});
  }

  commentModalVisible = (value,cId) =>{
    if(value == true){
      this.getCommentlist(cId);
      this.setState({ visibleCommentModal: value,
        select_complaint_id: cId});
    }else{
      this.setState({ visibleCommentModal: false,
        comment_discription: null,
        Commentlist:[],
        });
      this.getComplaintlist(cId);
    }
  }

  showLikelist = (value,cId) =>{
    if(value == true){
      this.getLikelist(cId);
      this.setState({ likeModal: value,
        select_complaint_id: cId});
    }else{
      this.setState({ likeModal: false,
        select_complaint_id:null,
        likeList:[],
        });
      this.getComplaintlist(cId);
    }
  }
  
  selectComplainttype = (value) =>{
    this.setState({btnSelected: value});
  }
  
  pickSingleWithCamera(cropping) {
    ImagePicker.openCamera({
      width: width,
      height: 140,
      cropping: cropping,
      includeBase64: true,
      includeExif: true,
      compressImageMaxWidth:300,
      compressImageMaxHeight:200
    }).then(image => {
      console.log(image)
      this.setState({
        image: {uri: `data:${image.mime};base64,`+ image.data, width: image.width, height: image.height},
        images: null
      });
    }).catch(e => console.log(e));
  }

  pickMultiple() {
    ImagePicker.openPicker({
      // multiple: true,
      waitAnimationEnd: false,
      includeExif: true,
      includeBase64: true,
      mediaType:"photo",
      width:width,
      height:140,
      selected1:"null",
      compressImageMaxWidth:300,
      compressImageMaxHeight:200

    }).then(image => {
      console.log(image)
      this.setState({
        
        image: {uri: `data:${image.mime};base64,`+ image.data, width: image.width, height: image.height},
        images: null
      });
    }).catch(e => console.log(e));
  }

  renderImage(image) {
    return <Image style={{width: width, height: 150 ,  flex: 1}} source={image} />
  }

  //==============================================================/
  //   set all  picker value  Changed in the state               //
  //==============================================================/
  onValueChange1 = value => {
    this.setState({
      selected1: value
    });
  };
  //=======================================================/
  //        function for Save Complaint (SaveComplaint)   //
  //=======================================================/
  complaintDiscription = (event) => {
    this.setState({ discription: event.nativeEvent.text })
  } 

  SaveComplaint = () => {
    if (this.state.selected1 == "null") {
      alert('Please select complaint category');
    } else if (this.state.discription == null) {
      alert('Please enter complaint discription');      
    }  else {
      this.setState({ isLoading: true });
        let data = JSON.stringify({
                                    customer_id:this.state.user_data.customer_id,
                                    customer_unit_id:this.state.customer_unit_id,
                                    category_id: this.state.selected1,
                                    description: this.state.discription,
                                    complaint_type:this.state.value,
                                    imageBase64:this.state.image.uri,
                                    property_id:constant.PROPERTY_ID,
                                  });        
          WebServiceHandler.SaveComplaint(data)
          .then(response => {    
          console.log(response);
          if (response.success) { 
           
            this.setState({ isLoading: false ,
              visibleModal:false,
              image:{uri:null} ,
              value: 0,
              discription:null});
              this.getComplaintlist();
          } else {   
            this.setState({ isLoading: false });                   
            let msg = response.message;
            alert(msg)
          }
        }).catch(error => {
          this.setState({ isLoading: false });
          alert(error)
        });    
    }
  } 
   //=======================================================/
  //        function for Save Comment (SaveComment)        //
  //=======================================================/
  commentDiscription = (event) => {
    this.setState({ comment_discription: event.nativeEvent.text })
  } 

  SaveComment = () => {
    if (this.state.comment_discription == '') {
      alert('Please enter comment');
    }  else {
      this.setState({ isLoading: true });
        let data = JSON.stringify({
                      complaint_id:this.state.select_complaint_id,
                      customer_id:this.state.user_data.customer_id,
                      type:1,
                      description:this.state.comment_discription,
                      status:1,
                  });        
          WebServiceHandler.SaveComment(data)
          .then(response => {    
          console.log(response);
          if (response.success) { 
            this.setState({ isLoading: false ,
              visibleCommentModal:false,
              image: null,
              images: null,
              value: 0,
              discription:null,
              comment_discription:''
              });
              this.getCommentlist(this.state.select_complaint_id);
          } else {   
            this.setState({ isLoading: false });                   
            let msg = response.message;
            alert(msg)
          }
        }).catch(error => {
          this.setState({ isLoading: false });
          alert(error)
        });    
    }
  } 
  
likePost = (item, index) =>{
  let Complaintlist  = this.state.Complaintlist;
  let targetPost = Complaintlist[index];
  console.log("targetPost",targetPost);
  console.log(targetPost.upVote);
  if(targetPost.upVote < 1){
    targetPost.upVote = 1;
    targetPost.upVoteCount = targetPost.upVoteCount + 1;
    let data = JSON.stringify({
      customer_id:this.state.user_data.customer_id,
      complaint_id:targetPost.complaint_id,
      type:0,   
      status:1 
    });
    WebServiceHandler.upvoteUpdate(data)
  }else{
    targetPost.upVote = 0;
    targetPost.upVoteCount = targetPost.upVoteCount -  1;
    let data = JSON.stringify({
      customer_id:this.state.user_data.customer_id,
      complaint_id:targetPost.complaint_id,
      type:0,   
      status:0 
    });
    WebServiceHandler.upvoteUpdate(data)
  } 
  Complaintlist[index] = targetPost;
  this.setState({ Complaintlist });
}

complaintDetailModal = (status, value) =>{
  this.setState({complaintDetail: value});
  this.setState({visibleDetailModal: value});
}

changeStatus = (value) =>{
  this.setState({
    status: value
  });
}

submitStatus = () => {
  alert(JSON.stringify(this.state.complaintDetail.complaint_id));
  alert(this.state.status);
}
   
  render() {
    return (
      <Container>
        <Content>
        { this.state.Complaintlist.map((value,i)=>(
              <Card style={{width:width,paddingRight:4}}>
                <TouchableOpacity onPress={() => this.complaintDetailModal(true, value)}>
                  <CardItem>
                    <Left>
                    {value.image != null ?<Thumbnail source={{uri: value.image}} />:<Thumbnail source = {{uri:'http://liveinloops.net/app/backoffice/public/default_avatar.jpg'}}/> }
                      
                      <Body>
                        <Text>{value.firstname}</Text>
                        <Text note>B-101</Text>
                        <Text note>{Moment.utc(value.created_at).local().format("L LT")}</Text>
                      </Body>
                    </Left>
                  </CardItem>
                  <CardItem cardBody>
                    <Body>
                      <Text style={{paddingLeft:7,paddingRight:5}}>{value.description}
                      </Text>
                    {value.complaintImg != null ? <Image  key={i} source={{uri:value.complaintImg}} style={{height: 200, width: width, flex: 1}}/>: null}
                    </Body>
                  </CardItem>
                </TouchableOpacity>
                <CardItem>
                  <View>
                    <Button transparent onLongPress={() => this.showLikelist(true,value.complaint_id)} onPress={() => this.likePost(value,i)}>
                    {value.upVote > 0  ? <FontAwesome  style={{color:'blue'}} size={25} active name="thumbs-up" />: <FontAwesome  style={{color:'grey'}} size={25} active name="thumbs-up" />} 
                      <Text style={{width:120}}>{value.upVoteCount} Votes</Text>
                    </Button>
                  </View>
                  <View>
                    <Button  transparent onPress={() => this.commentModalVisible(true,value.complaint_id)}>
                      <Foundation  style={{color:'blue'}} size={25}name="comments" />
                      <Text style={{width:180}}>{value.commentCOunt} Comments</Text>
                    </Button>
                  </View>
              </CardItem>
            </Card>
          // </ListItem>
          ))}
          {!this.state.isLoading ?
            <View> 
                  {this.state.Complaintlist.length > 0 ? null: <View><Text style={{textAlign:'center' ,marginTop:20}}>No complaint availabel</Text></View>
                  }
            </View> 
            :null
          }
        </Content>
        {/* Change password modal code start */}
        {this.state.visibleModal ?
        <View >
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.visibleModal}
              onRequestClose={() => {}}>
              <Header>
                <Left>
                  <Button
                    transparent
                    onPress={() => this.ModalVisible(!this.state.visibleModal)} >
                    <Icon name="arrow-back" />
                  </Button>
                </Left>
                <Body>
                  <Title style={{width:200}} >New complaint</Title>
                </Body>
                <Right />
              </Header>
              <Content style={{backgroundColor :"#c6c6c6"}} >
                <View style={{ padding: 10 }}>
                  <View style={{marginBottom: 5,backgroundColor:'white',padding:5}}>
                  <TouchableOpacity onPress={() => ActionSheet.show(
                                                                      {
                                                                        options: BUTTONS,
                                                                        cancelButtonIndex: CANCEL_INDEX,
                                                                        title: "Select Option"
                                                                      },
                                                                      buttonIndex => {
                                                                        if(BUTTONS[buttonIndex].value == 1){
                                                                          this.pickSingleWithCamera();
                                                                        }else if(BUTTONS[buttonIndex].value ==2){
                                                                          this.pickMultiple();
                                                                        }
                                                                      }
                                                                      )} >
                      
                        {this.state.image.uri ? [this.renderImage(this.state.image)] : <Image style={{width: width, height: 150 ,  flex: 1,}} source={require('../../media/images/copmplaint.jpg')} />}
                    </TouchableOpacity>
                  </View>
                  <View style={{marginBottom: 5,backgroundColor:'white'}}>
                     <View style={{padding:5,flexDirection:'row'}}>
                     <Item
                        
                        style={{ backgroundColor: "#fff",borderColor:'white' }}
                        regular
                      >
                        <Text>Category : </Text>
                        <Picker
                          style={{
                            width: width-120,
                            padding: 0,
                            margin:0
                          }}
                          iosIcon={<Icon style={{fontSize: 12}} name='chevron-down' />}
                          selectedValue={this.state.selected1}
                          placeholder="Select Category"
                          iosHeader="Select Category"
                          onValueChange={this.onValueChange1.bind(this)}
                        >
                          <Item label="Select Category" value="null" />
                          <Item label="Drainage" value="0" />
                          <Item label="cleaning" value="1" />
                          <Item label="Water supply" value="2" />
                        </Picker>
                        
                      </Item>
                     </View>
                  </View>
                  <View style={{marginBottom: 5,backgroundColor:'white'}}>
                     <Textarea rowSpan={5}  
                               placeholder="Discription......." 
                               returnKeyType={"done"}
                               value={this.state.discription}
                               onChange={this.complaintDiscription} 
                      />
                  </View>
                  <View style={{marginBottom: 5,backgroundColor:'white', height:50}}>
                     <View style={{padding:5,flexDirection:'row',marginTop:10}}>
                       <Text>Complaint Type:</Text> 
                        <RadioButton currentValue={this.state.value} value={0} onPress={this.handleOnPress.bind(this)}>
                          <Text style={{marginLeft:3}}>Private</Text>
                        </RadioButton>
                        <RadioButton  currentValue={this.state.value} value={1} onPress={this.handleOnPress.bind(this)}>
                          <Text style={{marginLeft:3}}>Public</Text>
                        </RadioButton>
                     </View>
                  </View>
                   
                </View>  
              </Content>
              <Footer>
               <Button transparent block  >
                    <Text uppercase={false} style={{  fontSize: 20, color:"white" }} onPress={()=> this.SaveComplaint()}>Submit</Text>
                </Button>
                
              </Footer>
            </Modal>
        </View>     
        :null
        }  
         {this.state.visibleCommentModal ?
        <View >
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.visibleCommentModal}
              onRequestClose={() => {}}>
              <Header>
                <Left>
                  <Button
                    transparent
                    onPress={() => this.commentModalVisible(!this.state.visibleCommentModal)} >
                    <Icon name="arrow-back" />
                  </Button>
                </Left>
                <Body>
                  <Title style={{width:200}} >Comments</Title>
                </Body>
                <Right />
              </Header>
              <Content>
                 {this.state.Commentlist.map((value,i)=>(
                    <List >
                      <ListItem >
                        {/* <Left>
                          {value.image != null ?<Thumbnail style={{height: 50, width: 50}} source={{ uri:value.image}} /> :<Thumbnail style={{height: 50, width: 50}} source={{ uri: 'http://liveinloops.net/app/backoffice/public/default_avatar.jpg' }} />}
                        </Left> */}
                        <Body>
                           <View style={{flexDirection:'row',justifyContent:'space-between',}}>
                            <Text style={{color:'blue'}}>{value.civilit}. {value.firstname} {value.lastname}</Text>
                              <Text  note>{Moment.utc(value.created_at).local().format("L LT")}</Text>
                          </View>
                          <View>
                          <Text >{value.description}</Text>
                          </View>
                        </Body>
                        {/* <View  style={{borderBottomColor:'grey',borderBottomWidth:'1'}}></View> */}
                      </ListItem>
                    </List>
                    
                 ))}
                 <View> 
                {this.state.Commentlist.length > 0 ?null : <View><Text style={{textAlign:'center' ,marginTop:20}}>No comments availabel</Text></View>
                }
              </View> 
              </Content>
              
              <Item regular> 
                <Input multiline={true}
                       placeholder="write a comment.."
                       returnKeyType={"done"}
                       value={this.state.comment_discription}
                       onChange={this.commentDiscription} 
                       /> 
                 <Button  style={{marginRight:10}} onPress={()=> this.SaveComment()} transparent>      
                <MaterialCommunityIcons size={30} style={{marginRight:10,color:'blue'}}  name="send" /> 
                </Button>
              </Item>
            </Modal>
        </View>     
        :null
        } 
         {this.state.likeModal ?
        <View >
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.likeModal}
              onRequestClose={() => {}}>
              <Header>
                <Left>
                  <Button
                    transparent
                    onPress={() => this.showLikelist(!this.state.likeModal)} >
                    <Icon name="arrow-back" />
                  </Button>
                </Left>
                <Body>
                  <Title style={{width:200}} >Upvotes</Title>
                </Body>
                <Right />
              </Header>
              <Content>
                 {this.state.likeList.map((value,i)=>(
                    <List >
                      <ListItem avatar>
                      <Left>
                        <Thumbnail source={{ uri: value.image != null? value.image:'http://liveinloops.net/app/backoffice/public/default_avatar.jpg' }} />
                      </Left>
                      <Body>
                        <Text>{value.civilit}. {value.firstname} {value.lastname}</Text>
                        <Text note>{Moment.utc(value.created_at).local().format("L LT")}</Text>
                      </Body>
                      <Right>
                      </Right>
                      </ListItem>
                    </List>
                 ))}
                 <View> 
                {this.state.likeList.length > 0 ?null : <View><Text style={{textAlign:'center' ,marginTop:20}}>No upvote for this complaint</Text></View>
                }
              </View> 
              </Content>
            </Modal>
        </View>     
        :null
        } 

        {this.state.visibleDetailModal ?
        <View>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.visibleDetailModal}
              onRequestClose={() => {}}>
              <Header>
                <Left>
                  <Button
                    transparent
                    onPress={() => this.complaintDetailModal(!this.state.visibleDetailModal)} >
                    <Icon name="arrow-back" />
                  </Button>
                </Left>
                <Body>
                  <Title style={{width:200}} >Detail</Title>
                </Body>
                <Right />
              </Header>
              <Content noBorder>
                  <Card noBorder style={{elevation: 0}}>
                    <CardItem>
                      <Left>
                      {this.state.complaintDetail.image != null ?<Thumbnail source={{uri: this.state.complaintDetail.image}} />:<Thumbnail source = {{uri:'http://liveinloops.net/app/backoffice/public/default_avatar.jpg'}}/> }
                        <Body>
                          <Text>{this.state.complaintDetail.firstname}</Text>
                          <Text note>B-101</Text>
                          <Text note>{Moment.utc(this.state.complaintDetail.created_at).local().format("L LT")}</Text>
                        </Body>
                      </Left>
                    </CardItem>
                    <CardItem cardBody style={{borderBottomWidth: 0}}>
                      <Body>
                      {this.state.complaintDetail.complaintImg != null ? <Image source={{uri: this.state.complaintDetail.complaintImg}} style={{height: 200, width: width, flex: 1}}/>: null}
                        <Text style={{padding: 20}}>
                          {this.state.complaintDetail.description}
                        </Text>
                      </Body>
                    </CardItem>
                    <Item style={{marginBottom: 15, borderBottomWidth: 0}}>
                      <Text style={{padding: 20}}>Change Status: </Text>
                      <Item rounded style={{width: 190}}>
                      <Picker selectedValue={this.state.status} style={{marginLeft: 20, marginRight: 20, borderWidth: 1}} 
                      onValueChange= {this.changeStatus.bind(this)}>
                        <Picker.Item label = "Pending" value = "0" />
                        <Picker.Item label = "In Progress" value = "1" />
                        <Picker.Item label = "Complete" value = "2" />
                      </Picker>
                      </Item>
                    </Item>
                  </Card>
              </Content>
              <Button block style={[{shadowOffset: {height: 0, width: 0},
                shadowOpacity: 0}]} 
                onPress={this.submitStatus} >
                <Text uppercase={false} style={commonstyle.buttoncolor}>Submit</Text>
              </Button>
            </Modal>
        </View>     
        :null
        } 

        <Fab
          active="true"
          containerStyle={{ }}
          style={{ backgroundColor: 'blue' }}
          position="bottomRight"
          onPress={() => this.ModalVisible(!this.state.visibleModal)}>
          <MaterialIcons name="add" />
        </Fab>
       {/* loader model */}
        <Modal
              transparent={true}
              animationType={'none'}
              visible={this.state.isLoading}
              onRequestClose={() => {}}>
              <View style={commonstyle.modalBackground}>
                <View style={commonstyle.activityIndicatorWrapper}>
                  <ActivityIndicator
                    animating={this.state.isLoading} size="large" color="black" />
                </View>
              </View>
         </Modal>
      </Container>
    );
  }
}