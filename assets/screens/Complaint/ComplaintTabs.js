import React, { Component } from 'react';
import { Dimensions, StatusBar, TouchableOpacity, ImageBackground} from 'react-native';
import { Button, Container, Header, Text, Left, Content,TabHeading, Body, Right,Tab, Tabs, Title, Icon, H2, Form, Item, Input, Picker } from "native-base";
import NewComplaint from "./NewComplaint";
import ProcessComplaint from "./ProcessComplaint";
import CompletedComplaint from "./CompletedComplaint";
const window = Dimensions.get("window");
export default class ComplaintTab extends Component { 
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = { tab:null,
                   screeHW: Dimensions.get('window')
                };
  }
  componentWillReceiveProps(nextprop){
    console.log("nextprop",nextprop);
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Header style={{ marginTop:window.height*0.028}}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.goBack()} >
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={{width:200}} >Complaints</Title>
          </Body>
          <Right />
        </Header>
       <Tabs  onChangeTab={({ i, ref }) => this.setState({tab : i}) } locked={true} initialPage={0}  tabBarUnderlineStyle={{backgroundColor:'#191970',borderBottomWidth:0}} >
          <Tab  heading="New"   activeTextStyle={{color: 'black', fontWeight: 'bold'}} textStyle={{color: 'black'}} activeTabStyle={{backgroundColor: '#d4d7ea'}} tabStyle={{backgroundColor: '#d4d7ea'}} >
            <NewComplaint tabProps={{screen:this.state.tab}} />
          </Tab>
          <Tab heading="Process"  activeTextStyle={{color: 'black', fontWeight: 'bold'}}  textStyle={{color: 'black'}} activeTabStyle={{backgroundColor: '#d4d7ea'}} tabStyle={{backgroundColor: '#d4d7ea'}}>
            <ProcessComplaint  tabProps={{screen:this.state.tab}} />
          </Tab>
          <Tab heading="Completed"  activeTextStyle={{color: 'black', fontWeight: 'bold'}} textStyle={{color: 'black'}} activeTabStyle={{backgroundColor: '#d4d7ea'}} tabStyle={{backgroundColor: '#d4d7ea'}}>
            <CompletedComplaint  tabProps={{screen:this.state.tab}} />
          </Tab>
        </Tabs>
     </Container>
    );
  }
}