import React, { Component } from "react";
import Complainttab from "./ComplaintTabs";
import { createDrawerNavigator } from "react-navigation";

const HomeScreenRouter = createDrawerNavigator(
  {
    ComplaintTab: { screen: Complainttab },
  },
  {
    contentComponent: props =>{props} 
  }
);
export default HomeScreenRouter;
