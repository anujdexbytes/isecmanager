import React, { Component } from 'react';
import { Dimensions, TouchableOpacity,  ImageBackground} from 'react-native'; 
import { Button, Container, Header, Text, View, Left, Content, Body, Right, Title, Icon, Fab } from "native-base";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import commonstyle from "../../commonstyle";
import Slideshow from 'react-native-slideshow';

const window = Dimensions.get("window");

let {width, height} = Dimensions.get('window');
export default class Homescreen extends Component { 
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = { tab:null,
                   screeHW: Dimensions.get('window'),
                   position: 1,
                   interval: null,
                   dataSource: [
                    {
                      title: 'Title 1',
                      caption: 'Caption 1',
                      url: require('../../media/home/1.jpg'),
                    }, {
                      title: 'Title 2',
                      caption: 'Caption 2',
                      url: require('../../media/home/2.jpg'),
                    }, {
                      title: 'Title 3',
                      caption: 'Caption 3',
                      url: require('../../media/home/3.jpg'),
                    },
                  ],
                };
  }

  componentWillMount() {
    this.setState({
      interval: setInterval(() => {
        this.setState({
          position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
        });
      }, 2000)
    });
  }

  componentWillUnmount() {
    clearInterval(this.state.interval);
  }

  componentWillReceiveProps(nextprop){
    console.log(nextprop.navigation.state.routeName)
  }
  renderScrenn = (title) =>{
     this.props.navigation.navigate(title);
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
    
      <Container style={{flex: 1}} >
          <Slideshow style={{height: 300}}
            dataSource={this.state.dataSource}
            position={this.state.position}
            onPositionChanged={position => this.setState({ position })} 
          > 
          </Slideshow>
          <Content>
          <View style={{marginVertical:height*0.08}}>
            <View style={{flex: 1, flexDirection: 'row', marginLeft:30, marginRight:30, marginBottom:30, justifyContent: 'space-between', position: 'relative', top: -10}}> 
              <View> 
                  <Button
                      style={[commonstyle.roundShapeIcon]} transparent
                      onPress={() => { this.renderScrenn("Complaint")} }
                    >
                      <SimpleLineIcons name="note"  size={30}  style={{textAlign:'center',justifyContent:'center'}} color={commonstyle.roundShapeIconColor.color} ><Text style={[commonstyle.roundShapeIcontext]}  onPress={() => { this.renderScrenn("Complaint")} }>{"\n"} {"\n"}COMPLAINT</Text></SimpleLineIcons>
                  </Button>
              </View> 
              <View>
                  <Button
                    style={[commonstyle.roundShapeIcon]} transparent
                    onPress={() => { this.renderScrenn("Notice")} }
                  >
                    <SimpleLineIcons name="notebook"  size={30}  style={{textAlign:'center',justifyContent:'center'}} color={commonstyle.roundShapeIconColor.color} ><Text style={[commonstyle.roundShapeIcontext]}  onPress={() => { this.renderScrenn("Notice")} }>{"\n"} {"\n"}NOTICE</Text></SimpleLineIcons>
                  </Button>
              </View>  
            </View>
            <View style={{flex: 1, flexDirection: 'row',marginLeft:30, marginRight:30, justifyContent: 'space-between', position: 'relative', top: -10}}> 
              <View>
                <Button style={[commonstyle.roundShapeIcon,{}]} transparent >   
                  <MaterialCommunityIcons name="human-male-female"  size={30}  style={{textAlign:'center',justifyContent:'center'}} color={commonstyle.roundShapeIconColor.color} >
                    <Text style={[commonstyle.roundShapeIcontext]}  onPress={() => { this.renderScrenn("Complaint")} }>{"\n"} {"\n"}VISITORS</Text>
                  </MaterialCommunityIcons>
                </Button>
              </View>  
              <View>
                <Button
                  style={[commonstyle.roundShapeIcon]} transparent
                  onPress={() => { this.renderScrenn("Directory")} }
                > 
                <MaterialIcons name="headset-mic"  size={30}  style={{textAlign:'center',justifyContent:'center'}} color={commonstyle.roundShapeIconColor.color} >
                  <Text style={[commonstyle.roundShapeIcontext]}  onPress={() => { this.renderScrenn("Directory")} }>{"\n"} {"\n"}DIRECTORY</Text>
                </MaterialIcons>
                </Button>
              </View>  
            </View>
          </View>
          </Content>
          <Fab
            active={false}
            containerStyle={{ marginLeft:-20,marginTop:-20}}
            style={{ backgroundColor: 'transparent', elevation: 0}}
            position="topLeft"
            onPress={() => this.props.navigation.openDrawer()}>
            <Icon style={{color:'#fff'}} name="menu" />
          </Fab>
          <Fab
            active={false}
            containerStyle={{ marginRight:-20,marginTop:-20}}
            style={{ backgroundColor: 'transparent', elevation: 0}}
            position="topRight">
            <MaterialCommunityIcons name="bell">
            </MaterialCommunityIcons>
          </Fab>
      </Container>
    );
  }
}