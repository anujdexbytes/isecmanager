export default {
 // =========================== CSS  FOR LOGIN FORGET PASSWORD SCREEN ==============================//
    container: {
      backgroundColor: "#FFF"
    },
    modalBackground: {
      flex: 1,
      alignItems: 'center',
      flexDirection: 'column',
      justifyContent: 'space-around',
      backgroundColor: '#00000040'
    },
    activityIndicatorWrapper: {
      backgroundColor: '#FFFFFF',
      height: 100,
      width: 100,
      borderRadius: 10,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-around'
    },
    iconstyle:{
        color: "white",
        position: 'absolute'
    },
    buttonbackgroundcolor:{
      backgroundColor: '#59e441'
    },
    loginputstyle:{
      marginLeft: 40,
      marginRight: 20,
      color:'white'
    },
    headerbackgroundcolor:{
      backgroundColor: '#ed872d'
    },
    headerbackcolor:{
      color: 'black',
    },
    headertitlecolor:{
      color: 'white',
      textAlign: 'center',
      fontSize:20
    },
    color:{
       color:'#ed872d'
    },
    decortationcolor:{
      textDecorationColor: '#ed872d',
      color:'#ed872d'
    },
    buttoncolor:{
      color: 'white',
      fontSize: 20
    },
    borderstyle:{
      borderColor:'white'
    },
    
   // =========================== CSS  FOR INTERNAL PAGE ==============================//
    innerContainer: {
      backgroundColor: "#191970"
    },
    mainContainer: {
      flex: 0.4,
      // backgroundColor: 'transparent',
      // justifyContent: 'center',
      // alignItems: 'center',
   },
   roundShapeIcon:{
    borderWidth:1,
    borderColor: '#fff',
    backgroundColor:'black',
    alignItems:'center',
    justifyContent:'center',
    width:135,
    height:135,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
   },
   roundShapeIcontext:{
    textAlign:'center',
    color:'#fff',

   },
   roundShapeIconColor:{
    color:'#fff'
   },

   btnSelected: {
     backgroundColor:'#3bc74e',
   },
   notSelected : {
     backgroundColor:'white',
   },
   textSelected: {
    color:'black',
  },
  textnotSelected : {
    color:'grey',
  },
  changepassiconstyle:{
    color: "blue",
    position: 'absolute'
 }, 
  
};
  