import React, { Component } from 'react';
import { StatusBar, Platform, AsyncStorage, ActivityIndicator, View } from 'react-native';
import { Root } from "native-base";
import { updateFocus } from "react-navigation-is-focused-hoc";
import { createStackNavigator , createDrawerNavigator} from 'react-navigation';
import Login from "./assets/screens/login/login";
import HomeScreen from "./assets/screens/HomeScreen/HomeScreen";
import SideBar from "./assets/screens/SideBar/SideBar"
import Complaint from "./assets/screens/Complaint/ComplaintTabs";
import Directory from "./assets/screens/Directory/DirectoryTabs";
import Notice from "./assets/screens/Notice/Notice";
import AboutUs from "./assets/screens/common screen/AboutUs";
import ChangePassword from "./assets/screens/common screen/ChangePassword";
import Editprofile from "./assets/screens/common screen/Editprofile";
import FAQ from "./assets/screens/common screen/FAQ";
import TermsConditions from "./assets/screens/common screen/TermsConditions";
import userDefaults from 'react-native-default-preference'
    const HomeScreenRouter = createDrawerNavigator(
      {
        HomeScreen :{screen:HomeScreen},
        SideBar:{screen:SideBar},
      },
      {
        contentComponent: props => <SideBar {...props} />
      }
    );

  const AppNavigator = createStackNavigator({
    Login: { screen: Login },
    HomeScreen :{screen:HomeScreenRouter},
    Complaint :{screen:Complaint},
    Notice :{screen:Notice},
    AboutUs :{screen:AboutUs},
    ChangePassword :{screen:ChangePassword},
    Editprofile :{screen:Editprofile},
    FAQ :{screen:FAQ},
    TermsConditions :{screen:TermsConditions},
    Directory :{screen:Directory},
    sidebar:{screen:HomeScreenRouter}
    },{ headerMode: 'none' }
  );

  const LoginAppNavigator = createStackNavigator({
    HomeScreen :{screen:HomeScreenRouter},
    Complaint :{screen:Complaint},
    Notice :{screen:Notice},
    AboutUs :{screen:AboutUs},
    ChangePassword :{screen:ChangePassword},
    Editprofile :{screen:Editprofile},
    FAQ :{screen:FAQ},
    TermsConditions :{screen:TermsConditions},
    Directory :{screen:Directory},
    Login: { screen: Login },
    sidebar:{screen:HomeScreenRouter}
   },{ headerMode: 'none' }
  );

export default class RootRoute extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
        loginStatus: '',
        isloading:1
    }
  }
  componentWillMount() {
     userDefaults.get("token").then((value) => {
      if(value != null && value != ''){ 
      this.setState({ loginStatus : true,
                      isloading:0
       });
      }else{
        this.setState({ loginStatus : '',
          isloading:0
        });
      }
     });
  }
  
  render() {
    return (
      <Root>
        { this.state.isloading == 1 ?
         <View style={{ flexDirection: 'row',
                        justifyContent: 'space-around',
                        padding: 10,
                        flex: 1,
                        justifyContent: 'center'}} >
        <ActivityIndicator size="large" color="black" />
        </View>
       :  this.state.loginStatus ?
        <LoginAppNavigator onNavigationStateChange={(prevState, currentState) => {
                                                      updateFocus(currentState);
                                                      console.log(currentState);
                                                      this.setState({screenName:currentState})
                                                    }}  
                                                    screenProps={{navigation:this.props.navigation}} />
        :
         <AppNavigator  onNavigationStateChange={(prevState, currentState) => {
                                                updateFocus(currentState);
                                                console.log(currentState);
                                                this.setState({screenName:currentState})
                                                }}  
                                                screenProps={{navigation:this.props.navigation}} />  }
      </Root>
    );
  }
}